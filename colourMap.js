
var scaleFactor = 1;
var xMin = 1, yMin =1;
var xMax = 1, yMax = 1;
var canvas;

var heatmap;
var markObjects;

var roomTexts;



var processData = function(data){

  globalMinima = -1;
  xMax = 10, xMin = -1;  
  yMax = 10, yMin = -1;  
  
  
  for(var i = 0; i<data.length; i++){
      for(var j =0 ; j<data[i].points.length; j++){        

        //x minima maxima
        if(data[i].points[j].x < xMin){
          xMin = data[i].points[j].x;
        } 

        if(data[i].points[j].x > xMax){
          xMax = data[i].points[j].x;
        }      

        //y minima  maxima
        if(data[i].points[j].y < yMin){
          yMin = data[i].points[j].y;
        }         

        if(data[i].points[j].y > yMax){
          yMax = data[i].points[j].y;
        }      

      }
  }  
  
  console.log("X ranges from "+ xMin+ " to "+ xMax);
  console.log("Y ranges from "+ yMin+ " to "+ yMax);  

  var realWidth = (xMax-xMin), realHeight = (yMax-yMin);

  console.log("width and height in real world ="+realWidth + " , "+ realHeight);
  console.log("width and height of canvas is "+ canvas.width +" , "+ canvas.height);
  
  var xScale = (canvas.width/realWidth), yScale = (canvas.height/realHeight);

  console.log("Scale factor for canvas/real is => "+ xScale +" , "+ yScale);

  

  scaleFactor = xScale < yScale ? xScale : yScale;

  console.log("final scale factor  chosen is "+ scaleFactor);

  // changing data for the base as 0 PROCESSING DATA
  for(var i = 0; i<data.length; i++){      
      var localminX = 5000, localminY = 5000;
      var localmaxX = -1, localmaxY = -1;

      for(var j =0; j<data[i].points.length; j++){
          //console.log("value of x = "+ data[i].points[j].x + " and y = "+ data[i].points[j].y);
          data[i].points[j].x = (data[i].points[j].x - xMin)*scaleFactor;
          data[i].points[j].y = (data[i].points[j].y - yMin)*scaleFactor;                  

          data[i].points[j].x = Math.round(data[i].points[j].x);
          data[i].points[j].y = Math.round(data[i].points[j].y);
          data[i].points[j].y = canvas.height - data[i].points[j].y;  

          //x Local minima maxima
          if(data[i].points[j].x < localminX){
            localminX = data[i].points[j].x;
          } 

          if(data[i].points[j].x > localmaxX){
            localmaxX = data[i].points[j].x;
          }      


          //y local minima  maxima
          if(data[i].points[j].y < localminY){
            localminY = data[i].points[j].y;
          } 

          if(data[i].points[j].y > localmaxY){
            localmaxY = data[i].points[j].y;
          }                
      }
      data[i].center.x = (localmaxX + localminX)/2;
      data[i].center.y = (localmaxY + localminY)/2;

      console.log("local minima is "+ localminX+" , "+ localminY);
      console.log("local maxima is "+ localmaxX+" , "+ localmaxY);      
      console.log("center of the shape at index "+ i+" is"+data[i].center.x+" , "+ data[i].center.y  );

  }


  return data;

}


var fetchObjects = function(callback){
   canvas = new fabric.Canvas('c1');
  //raw data from ajax call and their location
  //defalt colur #0E0C0C
  
  var rawdata = [ {name: "Bathroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: -1580},
                                                              {x: -2000, y: -1580},
                                                              {x: -2000, y: -3450},
                                                              {x: 0, y: -3450}]  },

                {name: "Bedroom", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0},
                                                              {x: -2000, y: 0},
                                                              {x: -2000, y: 3370}, 
                                                              {x: 0, y: 3370}]  },

                {name: "Corridor", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                               {x: -2000, y: 0}, 
                                                               {x: -2000, y: -1580}, 
                                                               {x: 0, y: -1580}]  },

                {name: "Kitchen", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                              {x: 2980, y: 0}, 
                                                              {x: 2980, y: -3450}, 
                                                              {x: 0, y: -3450}]  },

                {name: "Living room", colour: "black", center: {x: "", y: ""}, points: [{x: 0, y: 0}, 
                                                                  {x: 2980, y: 0}, 
                                                                  {x: 2980, y: 3370}, 
                                                                  {x: 0, y: 3370}]  } ];  

  var data = processData(rawdata);   

  //i start from here.

  for(var i =0; i<data.length; i++){
     for (var j = 0; j< data[i].points.length; j++){ 
      //console.log("value of x = "+ data[i].points[j].x); 
      //console.log("   value of y = "+ data[i].points[j].y); 
    }
  }
  
  roomTexts = new Array();
  
  var shape_grp = new fabric.Group([], { id: "rooms", left: 0, top: 0, scaleX: 1, scaleY: 1});                

  for(var i =0; i<data.length; i++){
    var coordinates = data[i].points;
    var colour = data[i].colour;
    var center = data[i].center;
    //also add text here      
    var text = new fabric.Text(data[i].name, { id: "text", left: center.x, top: center.y, originX: 'center', originY: 'center', fontSize: 20,fill: 'black',opacity: 1}); 
    roomTexts.push(text);
    //text array updated

    var str = getPathString(rawdata[i].points);
    console.log(" value of string AT INDEX "+i+" is "+str+"\n");                                                                  
    var shape = new fabric.Path(str, {  originX: 'center', originY: 'center', opacity: 0.7, fill: colour, strokeWidth: 1, stroke: 'rgba(0,0,0,1)'}); 
    shape_grp.add(shape);  
  }

  //var bedroomS = new fabric.Path('M  0 0 L 0 200 L 200 200 L 200 0 z', {  originX: 'center', originY: 'center', opacity: 0.5, fill: 'red'});                 
  //var badS = new fabric.Path('M  0 200 L 200 200 L 200 300 L 0 300 z', {  originX: 'center', originY: 'center', opacity: 1, fill: 'yellow'});                     
  callback(shape_grp, roomTexts);
   
}


/**
* helper function to get the 
*
**/

var getPathString = function(points){
  
  var str = "M"
  for(var i = 0;; i++){
    
    if( i== points.length){
      str = str + " z";    
      break;
    }

    str = str + " "+points[i].x +" " +points[i].y;
    if((i+1) !== points.length){
      str= str + " L"
    }


  }  
  return str;
}

/**
*   ENTERY POINT FOR Background drawing
*
*/
var createMap   = function () {
  fetchObjects(function(rooms, texts){
    // adding rooms to canvas
    canvas.add(rooms);
    //text coordinate are calculated seperately
    for (var i = 0; i < texts.length; i++) {
      canvas.add(texts[i]);
    };  
    //canvas.add(textb);        
    //canvas.add(textbad);        
    canvas.renderAll();        
  });      
           
}


var toggleNames = function(enable){

  if(enable==0){
    for(var i = 0; i < roomTexts.length; i++){
      if(canvas.contains(roomTexts[i])){
        canvas.remove(roomTexts[i]);          
      }
      else{
        console.log("No element on sreen to delete.");
        return;
      }              
    }        
  }
  //in case to display it on screen.
  else{
    for(var i = 0; i < roomTexts.length; i++){
      
      if(!canvas.contains(roomTexts[i])){
        canvas.add(roomTexts[i]);          
      }
      else{
        console.log("Already there");
        return;
      }              
    }    
  }
  setTimeout(function(){canvas.renderAll();}, 100);  
}
  


/*
**  Helper function for the Heat maps
*/

var updateLegend = function (data) {

  var legendCanvas = document.createElement('canvas');
  legendCanvas.width = 100;
  legendCanvas.height = 10;

  var legendCtx = legendCanvas.getContext('2d');
  var gradientCfg = {};
  function $(id) {
    return document.getElementById(id);
  };

  // the onExtremaChange callback gives us min, max, and the gradientConfig
  // so we can update the legend
  //$('min').innerHTML = data.min;
  //$('max').innerHTML = data.max;
  $('min').innerHTML = "1";
  $('max').innerHTML = "10";

  // regenerate gradient image
  if (data.gradient != gradientCfg) {
    gradientCfg = data.gradient;
    var gradient = legendCtx.createLinearGradient(0, 0, 100, 1);
    for (var key in gradientCfg) {
      gradient.addColorStop(key, gradientCfg[key]);
    }

    legendCtx.fillStyle = gradient;
    legendCtx.fillRect(0, 0, 100, 10);
    $('gradient').src = legendCanvas.toDataURL();
  }
};

var createHeatMap = function(){

    heatmap = h337.create({
    container: document.getElementById("heatmap"),
    maxOpacity: .7,
    radius: 500*scaleFactor,
    blur: 0.85,
    // update the legend whenever there's an extrema change
    onExtremaChange: function onExtremaChange(data) {
      updateLegend(data);
    }
  });

};

 
var fetchObjects_heatData = function(callback){

  //camefrom database
  var raw_data = [{event_id: 2, event_name: "Sitting", points:[ { x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14},{ x: 2154, value: 1, y: 14}, { x: 2154, value: 1, y: 14}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                  {event_id: 3, event_name: "Sleeping", points:[{x: 154, y: 414}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                  {event_id: 4, event_name: "Lying", points:[{x: 2154, y: 14}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                  {event_id: 9, event_name: "Drinking", points:[{x: 2154, y: 14}, {x: 2189, y: 503}, {x: 126, y: 2239}, {x: -653, y: 355}]},
                  ];
    
  //processed

  for(var i = 0; i< raw_data.length; i++){
    
    for(var j = 0; j< raw_data[i].points.length; j++){
      raw_data[i].points[j].x = (raw_data[i].points[j].x - xMin)*scaleFactor;
      raw_data[i].points[j].y = (raw_data[i].points[j].y - yMin)*scaleFactor;

    }
    
  }
  markObjects = raw_data;
  callback(raw_data);                  

};


/**
* Object contains array of array 
*
*/
var swichPlots = function(event_id){
  console.log("initial value is "+ markObjects[event_id].points.length);
  
  heatmap.setData({    
    max: markObjects[event_id].points.length,
    min:0,
    data: markObjects[event_id].points
    //data: [{ x: 0, y: 0, value: 1}, { x: 100, y: 25, value: 1}, { x: 100, y: 25, value: 1}, { x: 110, y: 25, value: 4}, { x: 120, y: 25, value: 1}, { x: 100, y: 225, value: 1}]
  });

  var maxV = -1;
  for (var xx = 0; xx < canvas.width; xx++) {
    for (var yy = 0;yy < canvas.height; yy++) {
      var val = heatmap.getValueAt({x: xx,y: yy});
      if (val > maxV){
        maxV = val;
      } 
    }
  }
  console.log("max value is "+ maxV);
  heatmap.setData({    
    max: maxV,
    min:0,
    data: markObjects[event_id].points
    //data: [{ x: 0, y: 0, value: 1}, { x: 100, y: 25, value: 1}, { x: 100, y: 25, value: 1}, { x: 110, y: 25, value: 4}, { x: 120, y: 25, value: 1}, { x: 100, y: 225, value: 1}]
  });

  console.log("value of TEST: "+ heatmap.getValueAt({x: (2154-xMin)*scaleFactor, y: (14-yMin)*scaleFactor}) );
}

var generate = function() {

  fetchObjects_heatData(function(data){

    //checking data after all processing
    for(var i =0; i<data.length; i++){
       for (var j = 0; j< data[i].points.length; j++){ 
        console.log("value of x = "+ data[i].points[j].x); 
        console.log("   value of y = "+ data[i].points[j].y); 
      }
    }
    swichPlots(0);
  });
}