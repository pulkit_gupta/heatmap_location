var data = [ {name: "Bathroom", colour: "#0E0C0C", points: [{x: 0, y: -1580},
                                                              {x: -2000, y: -1580},
                                                              {x: -2000, y: -3450},
                                                              {x: 0, y: -3540}]  },

                {name: "Bedroom", colour: "#0E0C0C", points: [{x: 0, y: 0},
                                                              {x: -2000, y: 0},
                                                              {x: -2000, y: 3370}, 
                                                              {x: 0, y: 3370}]  },

                {name: "Corridor", colour: "#0E0C0C", points: [{x: 0, y: 0}, 
                                                               {x: -2000, y: 0}, 
                                                               {x: -2000, y: -1580}, 
                                                               {x: 0, y: -1580}]  },

                {name: "Kitchen", colour: "#0E0C0C", points: [{x: 0, y: 0}, 
                                                              {x: 2980, y: 0}, 
                                                              {x: 2980, y: -3450}, 
                                                              {x: 0, y: -3450}]  },

                {name: "Living room", colour: "#0E0C0C", points: [{x: 0, y: 0}, 
                                                                  {x: 2980, y: 0}, 
                                                                  {x: 2980, y: 3370}, 
                                                                  {x: 0, y: 3370}]  } ];




  var markPoints = [   
                      {id: 1, name: "sleeping", image: "sleeping.jpg", location: [{left: 10, top: 400}, {left: 12, top: 4}, {left: 120, top: 200}, {left: 122, top: 24}]},

                      {id: 2, name: "Fallen", image: "falling.png", location: [{left: 200, top: 12}, {left: 33, top: 24}, {left: 400, top: 142}, {left: 343, top: 424}, {left: 240, top: 122}, {left: 334, top: 242}]},

                      {id: 3, name: "standing", image: "standing.png", location: [{left: 100, top: 112}, {left: 121, top: 142}, {left: 500, top: 112}, {left: 621, top: 142}, {left: 551, top: 112}, {left: 351, top: 142}]},

                      {id: 4, name: "Drinking", image: "drinking.png", location: [{left: 310, top: 332}, {left: 212, top: 314}, {left: 410, top: 32}, {left: 552, top: 34}, {left: 510, top: 32}, {left: 712, top: 14}]}];